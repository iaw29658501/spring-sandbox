package com.example.secondteacher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class SecondTeacherApplication {

    public static void main(String[] args) {

        ConfigurableApplicationContext context = SpringApplication.run(SecondTeacherApplication.class, args);

    }

}
