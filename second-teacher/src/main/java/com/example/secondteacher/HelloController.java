package com.example.secondteacher;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @RequestMapping("/nice")
    public String hello(){
        return "nice";
    }
}
