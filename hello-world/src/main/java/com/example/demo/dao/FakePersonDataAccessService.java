package com.example.demo.dao;

import com.example.demo.model.Person;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository("fakeDao")
public class FakePersonDataAccessService implements PersonDao{
    private static List<Person> DB = new ArrayList<>();
    @Override
    public int addPerson(UUID id, Person person) {
        DB.add(new Person(id,person.getName()));
        return 1;
    }

    @Override
    public int addPerson(Person person) {
        return PersonDao.super.addPerson(person);
    }

    @Override
    public Optional<Person> getPerson(UUID id) {
        return DB.stream().filter(person -> person.getId().equals(id))
                .findFirst();
    }

    @Override
    public List<Person> selectAllPeople() {
        return DB;
    }

    @Override
    public Optional<Person> selectPersonById(UUID id) {
        return DB.stream()
                .filter(person -> person.getId().equals(id) )
                .findFirst();
    }

    @Override
    public int deletePersonById(UUID id) {
        Optional<Person> personOptional = getPerson(id);
        if ( personOptional.isEmpty()) {
            return 0;
        }
        DB.removeIf( person -> person.getId().equals(id));
        return 1;
    }

    @Override
    public int updatePersonById(UUID id, Person person) {
        Optional<Person> personOptional = getPerson(id);
        if ( personOptional.isEmpty()) {
            return 0;
        }
        int indexOfPersonToUpdate = DB.indexOf(personOptional.get());
        if (indexOfPersonToUpdate >= 0 ) {
            DB.set(indexOfPersonToUpdate, new Person(id, person.getName()));
            return 1;
        }
        return 0;
    }
}
